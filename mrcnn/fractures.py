import os
import sys
import random
import re
import numpy as np
import matplotlib.pyplot as plt
import cv2
import skimage.io
import re
import warnings

def compute_dice_coefficient(mask_gt, mask_pred):
  volume_sum = mask_gt.sum() + mask_pred.sum()
  
  if volume_sum == 0:
    return np.NaN
  volume_intersect = (np.bitwise_and(mask_gt , mask_pred)).sum()
  return 2*volume_intersect / volume_sum 

# filterwarnings(action='ignore', category=DeprecationWarning, message='`np.bool` is a deprecated alias')
# filterwarnings(action='ignore', category=UserWarning, message='`np.bool` is a deprecated alias')
warnings.filterwarnings('ignore')
# Root directory of the project
ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from config import Config
import utils
import model as modellib
import visualize as visualize
from model import log
VAL_IMAGES_LENGTH=0
TRAIN_IMAGES_LENGTH=0
# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
# Download COCO trained weights from Releases if needed
if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)


imagePathOutput='/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindowSplit'
valDir='val/DataEmpty'
trainDir='train/DataEmpty'
testDir='test/DataEmpty'


def visualize_images(img1,img2):
    f = plt.figure(figsize=(15, 10))
    f.add_subplot(1,2, 1)
    plt.imshow(img1,cmap='gray')
    f.add_subplot(1,2, 2)
    plt.imshow(img2,cmap='gray')
    plt.show(block=True)

dataset_dir = os.path.join(imagePathOutput, trainDir)
image_ids = next(os.walk(dataset_dir))[1]
all_images_train=[]
for image_id in image_ids:
    im_dir=os.path.join(dataset_dir, image_id)
    all_images_train.extend(next(os.walk(im_dir))[2])

dataset_dir = os.path.join(imagePathOutput, valDir)
image_ids = next(os.walk(dataset_dir))[1]
all_images_val=[]
for image_id in image_ids:
    im_dir=os.path.join(dataset_dir, image_id)
    all_images_val.extend(next(os.walk(im_dir))[2])

def get_ax(rows=1, cols=1, size=10):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.
    
    Adjust the size attribute to control how big to render images
    """
    fig, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
    fig.tight_layout()
    return ax
def get_ax(rows=1, cols=1, size=10):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.
    
    Adjust the size attribute to control how big to render images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
    return ax


class FracturesConfig(Config):
    """Configuration for training on the Rib Fractures dataset.
    Derives from the base Config class and overrides values specific
    to the Rib Fractuers dataset.
    """
    # Give the configuration a recognizable name
    NAME = "Fractures"

    # Train on 1 GPU and 7 images per GPU. We can put multiple images on each
    # GPU because the images are small. Batch size is 7 (GPUs * images/GPU).
    GPU_COUNT = 1
    IMAGES_PER_GPU = 3
    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # background + 1 fractures
    DETECTION_MIN_CONFIDENCE = 0
    
    # IMAGE_CHANNEL_COUNT = 1
    # # Image mean (RGB)
    # MEAN_PIXEL = np.array([127.5])

    # Backbone network architecture
    # Supported values are: resnet50, resnet101
    BACKBONE = "resnet50"

    # Input image resizing
    # Random crops of size 512x512
    # IMAGE_RESIZE_MODE = "crop"
    # IMAGE_MIN_DIM = 512
    # IMAGE_MAX_DIM = 512
    IMAGE_MIN_SCALE = 2.0

    # Length of square anchor side in pixels
    RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)

    # ROIs kept after non-maximum supression (training and inference)
    POST_NMS_ROIS_TRAINING = 1000
    POST_NMS_ROIS_INFERENCE = 2000

    # Non-max suppression threshold to filter RPN proposals.
    # You can increase this during training to generate more propsals.
    RPN_NMS_THRESHOLD = 0.9

    # How many anchors per image to use for RPN training
    RPN_TRAIN_ANCHORS_PER_IMAGE = 64


    # If enabled, resizes instance masks to a smaller size to reduce
    # memory load. Recommended when using high-resolution images.
    USE_MINI_MASK = True
    MINI_MASK_SHAPE = (56, 56)  # (height, width) of the mini-mask
    
    # Number of ROIs per image to feed to classifier/mask heads
    # The Mask RCNN paper uses 512 but often the RPN doesn't generate
    # enough positive proposals to fill this and keep a positive:negative
    # ratio of 1:3. You can increase the number of proposals by adjusting
    # the RPN NMS threshold.
    TRAIN_ROIS_PER_IMAGE = 128

    # Maximum number of ground truth instances to use in one image
    # MAX_GT_INSTANCES = 200
    MAX_GT_INSTANCES = 100

    # Max number of final detections per image
    # DETECTION_MAX_INSTANCES = 400
    DETECTION_MAX_INSTANCES = 100

    # STEPS_PER_EPOCH = 42118// IMAGES_PER_GPU
    # VALIDATION_STEPS = 4411 // IMAGES_PER_GPU
    # STEPS_PER_EPOCH = 1000
    # VALIDATION_STEPS = 100
    
    
config = FracturesConfig()
# config.display()

class FracturesDataset(utils.Dataset):

    # def load_image(self, image_id):
    #     """Load the specified image and return a [H,W,3] Numpy array.
    #     """
    #     # Load image
    #     image = skimage.io.imread(self.image_info[image_id]['path'],as_gray=True)
    #     # image=cv2.imread(self.image_info[image_id]['path'],0)
    #     image = image[..., np.newaxis]

        
    #     return image

    # 2.5D?
    # def load_image(self, image_id):
    #     """Load the specified image and return a [H,W,3] Numpy array.
    #     """
    #     # Load image
    #     filename=self.image_info[image_id]['id'].split('-')[1]
    #     num = int(re.findall(r"\d+", filename)[0])
    #     filename_1=self.image_info[image_id-1]['id'].split('-')[1]
    #     num_M = int(re.findall(r"\d+", filename_1)[0])
    #     filename_2=self.image_info[image_id+1]['id'].split('-')[1]
    #     num_P = int(re.findall(r"\d+", filename_2)[0])

    #     image = skimage.io.imread(self.image_info[image_id]['path'])
        
    #     # If grayscale. Convert to RGB for consistency.
    #     if image.ndim != 3:
    #         image = skimage.color.gray2rgb(image)
    #         if num_M==num-1:
    #             image[:,:,0]=skimage.io.imread(self.image_info[image_id-1]['path'])
    #         if num_P==num+1:
    #             image[:,:,2]=skimage.io.imread(self.image_info[image_id+1]['path'])
    #     # If has an alpha channel, remove it for consistency

    #     return image

    def load_scan(self, dataset_dir, subset,all_images):
        """Load a subset of the ribfrac dataset.
        dataset_dir: Root directory of the dataset
        subset: Subset to load.
                * train: training data
                * val: validation data
                * test: test data
        all-images: list contating all slice names.
        """
        # Add classes. We have one class.
        # Naming the dataset farcture, and the class fracture
        self.add_class("fracture", 1, "fracture")
        dataset_dir = os.path.join(dataset_dir, subset)
        
        # Add images by loopping through each CT scan directory and getting all its slices
        all_images=sorted(all_images)
        for image_id in all_images:
            filename=image_id.split('-')[0]
            num = re.findall(r"\d+", filename)[0]

            # if the image ends with e.png then its corresponding mask is empty ,so its skipped
            Rand_Neg=random.random()

            # 1:1 ratio of poistive and negative samples
            # if image_id.endswith('png') and ((not image_id.endswith('e.png')) or Rand_Neg>0.6):

            # Only all positive samples
            # if image_id.endswith('png') and (not image_id.endswith('e.png')):

            # All Samples
            if image_id.endswith('png') :
                self.add_image(
                    "fracture",
                    image_id=image_id,
                    path=os.path.join(dataset_dir, "RibFrac"+num,image_id))

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        info = self.image_info[image_id]
        
        # Get mask directory from image path
        head,tail=os.path.split(os.path.dirname(os.path.dirname(info['path'])))
        mask_dir = os.path.join(head, "MaskOne")
        filename=info['id'].split('-')[0]
        num = re.findall(r"\d+", filename)[0]
        
        # Read mask files from .png image
        if (info['id'].endswith("e.png")):
            info['id'] = re.sub('e.png$', '.png', info['id'])
        mask= skimage.io.imread(os.path.join(mask_dir, "RibFrac"+num,info['id']))

        # Get different fracture values to create instances
        uniq=np.unique(mask)
        masks=[]

        if len(uniq)>1:
            for val in uniq:
                # create a mask instance for each fracture in a mask to add it to a list
                if val !=0:
                    filt_mask=np.where(mask!=val, 0, mask)
                    masks.append(filt_mask.astype(np.bool))
            masks = np.stack(masks, axis=-1)
            return masks, np.ones([masks.shape[-1]], dtype=np.int32)
        else:
            masks.append(mask.astype(np.bool))
            masks = np.stack(masks, axis=2)
            return masks, np.zeros(1, dtype=np.int32)
            
# Training dataset
# random.shuffle(all_images_train)
# random.shuffle(all_images_val)

dataset_train = FracturesDataset()
print("Trainging Dataset.....................")
dataset_train.load_scan(imagePathOutput,trainDir,all_images_train)
dataset_train.prepare()
TRAIN_IMAGES_LENGTH=len(dataset_train.image_info)
print(TRAIN_IMAGES_LENGTH)
#  Validation dataset
print("Validation Dataset..........................")
dataset_val = FracturesDataset()
dataset_val.load_scan(imagePathOutput,valDir,all_images_val)
dataset_val.prepare()
VAL_IMAGES_LENGTH=len(dataset_val.image_info)
print(VAL_IMAGES_LENGTH)

# image_ids = np.random.choice(dataset_train.image_ids, 1)
# for image_id in image_ids:
#     pic=dataset_train.load_image(image_id)
#     visualize_images(dataset_train.load_image(image_id),dataset_train.load_image(image_id))


train=0
if train==1:
    model = modellib.MaskRCNN(mode="training", config=config,
                              model_dir=MODEL_DIR)


    # Which weights to start with
    init_with = "imagenet"  # imagenet, coco, or last

    if init_with == "imagenet":
        from tensorflow.keras.utils import get_file
        TF_WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/'\
                                 'releases/download/v0.2/'\
                                 'resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5'
        weights_path = get_file('resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                TF_WEIGHTS_PATH_NO_TOP,
                                cache_subdir='models',
                                md5_hash='a268eb855778b3df3c7506639542a6af')
        # model.load_weights(model.get_imagenet_weights(), by_name=True,exclude=["conv1"])
        model.load_weights(weights_path, by_name=True)
    elif init_with == "coco":
        # Load weights trained on MS COCO, but skip layers that
        # are different due to the different number of classes
        # See README for instructions to download the COCO weights
        model.load_weights(COCO_MODEL_PATH, by_name=True,
                           exclude=["mrcnn_class_logits", "mrcnn_bbox_fc", 
                                    "mrcnn_bbox", "mrcnn_mask"])
    elif init_with == "last":
        # Load the last model you trained and continue training
        # model_path = os.path.join("/Users/omarhashish/Desktop/logs/fractures20220413T0605", "mask_rcnn_fractures_0008.h5")
        # model_path = os.path.join("/Users/omarhashish/Desktop/logs/gamed", "mask_rcnn_fractures_0003.h5")
        # model.load_weights(model_path, by_name=True)
        # model.load_weights(model.find_last(), by_name=True)
        # model_path = os.path.join("/Users/omarhashish/Desktop/logs/fractures20220419T1149", "mask_rcnn_fractures_0009.h5")
        model_path = os.path.join("/Users/omarhashish/Desktop/logs/fractures20220421T1855", "mask_rcnn_fractures_0021.h5")
        model.load_weights(model_path, by_name=True)

    from imgaug import augmenters as iaa
    #add agumentation of flipping horizontally 50% of the images 
    # augmentation = iaa.SomeOf((0, 1), [
    #     iaa.Fliplr(0.5),
    #     iaa.Flipud(0.5),
    #     iaa.OneOf([iaa.Affine(rotate=90),
    #                iaa.Affine(rotate=180),
    #                iaa.Affine(rotate=270)])
    #     # ,iaa.Multiply((0.8, 1.5)),
    #     # iaa.GaussianBlur(sigma=(0.0, 5.0))
    # ])

    augmentation = iaa.Fliplr(0.5)
        
    #train by all netword except first layer
    import tensorflow as tf

    import datetime
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=0,
    write_graph=True, write_images=True)
    # model.train(dataset_train, dataset_val, 
    #             learning_rate=config.LEARNING_RATE / 10,
    #             epochs=10,
    #             augmentation=augmentation,
    #             custom_callbacks=[tensorboard_callback],
    #             layers="heads")

    model.train(dataset_train, dataset_val, 
                learning_rate=config.LEARNING_RATE / 10,
                epochs=20,
                augmentation=augmentation,
                custom_callbacks=[tensorboard_callback],
                layers="all")


viz=1
if viz==1:
    dataset_dir = os.path.join(imagePathOutput, testDir)
    image_ids = next(os.walk(dataset_dir))[1]
    all_images_test=[]
    for image_id in image_ids:
        im_dir=os.path.join(dataset_dir, image_id)
        all_images_test.extend(next(os.walk(im_dir))[2])

    # Test dataset
    print("Test Dataset..........................")
    dataset_test = FracturesDataset()
    dataset_test.load_scan(imagePathOutput,testDir,all_images_test)
    dataset_test.prepare()

    class InferenceConfig(FracturesConfig):
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1
        # USE_MINI_MASK = True
        # Don't resize imager for inferencing
        # IMAGE_RESIZE_MODE = "pad64"
        # Non-max suppression threshold to filter RPN proposals.
        # You can increase this during training to generate more propsals.
        # IMAGE_MIN_SCALE = 2.0
        DETECTION_MIN_CONFIDENCE = 0
        RPN_NMS_THRESHOLD = 0.7
    inference_config = InferenceConfig()

    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference", 
                            config=inference_config,
                            model_dir=MODEL_DIR)

    # Get path to saved weights
    # Either set a specific path or find last trained weights
    model_path = os.path.join("/Users/omarhashish/Desktop/logs/fractures20220421T1855", "mask_rcnn_fractures_0021.h5")
    # model_path = os.path.join("/Users/omarhashish/Desktop/logs/fractures20220430T0036", "mask_rcnn_fractures_0013.h5")
    
    # model_path = model.find_last() 
    # # Load trained weights
    print("Loading weights from ", model_path)
    model.load_weights(model_path, by_name=True)


    ##########################################
    # PREDICTION
    ##########################################
    for pp in range(500,501):
        rib_choice =pp

        Des_Im=[]
        for i,item in enumerate(dataset_test.image_info):
            le_img=dataset_test.image_info[i]["id"]
            filename=le_img.split('-')[0]
            num = re.findall(r"\d+", filename)[0]
            if int(num)==rib_choice:
                Des_Im.append(le_img)
        Des_Im=sorted(Des_Im)

        import nibabel as nib

        mask3D=f'RibFrac{rib_choice}-label.nii.gz'
        data3D = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow/Mask/'+mask3D
        affine= nib.load(data3D).affine
        img = nib.load(data3D).get_fdata()
        
        recon= (np.full_like(img, 0)).astype(int)
        slice_num=0
        # visualize_images(img[:,:,150],img[:,:,150])
        # image_id = random.choice(dataset_test.image_ids)
        for j in Des_Im:
            # if not j.endswith("e.png"):
            for i in dataset_test.image_ids:
                if dataset_test.image_info[i]["id"]==j:
                    image_id=i
                    # # # # # Detect objects
                    # #DONT FORGET TO LOAD ALL SAMPLES

                    image = dataset_test.load_image(image_id)
                    r = model.detect([image], verbose=0)[0]
                    slice_2D=r['masks'].astype(int)
                    if slice_2D.shape[2]>0:
                        for i in range(slice_2D.shape[2]):
                            recon[:,:,slice_num]=recon[:,:,slice_num]+np.rot90(slice_2D[:,:,i],3)
                    print(slice_num)
                    print(slice_2D.shape)
                    slice_num=slice_num+1
                
                    ####################################################################################
                    # Get predictions of mask head
                    ####################################################################################
                    # # # Run object detection
                    # image_id = np.random.choice(dataset_test.image_ids, 10)
                    # image, image_meta, gt_class_id, gt_bbox, gt_mask =\
                    #     modellib.load_image_gt(dataset_test, inference_config, image_id[0], use_mini_mask=False)
                    # info = dataset_test.image_info[image_id[0]]
                    # print("image ID: {}.{} ({}) {}".format(info["source"], info["id"], image_id[0], 
                    #                                     dataset_test.image_reference(image_id[0])))
                    # print("Original image shape: ", modellib.parse_image_meta(image_meta[np.newaxis,...])["original_image_shape"][0])
                    # results = model.detect([image], verbose=1)
                    # r = results[0]

                    # # Display results

                    # # image=resize_image(image,512,512)
                    # print(r['masks'].shape)
                    # visualize.display_differences(
                    #     image,
                    #     gt_bbox, gt_class_id, gt_mask,
                    #     r['rois'], r['class_ids'], r['scores'], r['masks'],
                    #     dataset_test.class_names, ax=get_ax(1),
                    #     show_box=True, show_mask=True,
                    #     iou_threshold=0.2, score_threshold=0.5)
                    # plt.show()

                    # # OR

                    # visualize.display_instances(
                    #     image, r['rois'], r['masks'], r['class_ids'],
                    #     dataset_test.class_names, r['scores'],
                    #     show_bbox=True, show_mask=True,ax=get_ax(1),
                    #     title="Predictions "+dataset_test.image_info[image_id]["id"])
                    # plt.show()
                    # visualize.display_instances(
                    #     image, gt_bbox, gt_mask, gt_class_id,
                    #     dataset_test.class_names,
                    #     show_bbox=True, show_mask=True,ax=get_ax(1),
                    #     title="Predictions "+dataset_test.image_info[image_id]["id"])
                    # plt.show()

                    ####################################################################################
                    #Calculate Accuracy
                    ####################################################################################
                    # import datetime
                    # image_ids = np.random.choice(dataset_test.image_ids, 100)
                    # APs = []
                    # fastb=[]
                    # for image_id in image_ids:
                    #     # Load image and ground truth data
                    #     image, image_meta, gt_class_id, gt_bbox, gt_mask =\
                    #         modellib.load_image_gt(dataset_test, inference_config,
                    #                             image_id, use_mini_mask=False)
                    #     molded_images = np.expand_dims(modellib.mold_image(image, inference_config), 0)
                    #     # Run object detection
                    #     results = model.detect([image], verbose=0)

                    #     r = results[0]
                    #     # diceco=compute_dice_coefficient((gt_mask.sum(axis=2)).astype(np.int8),(r['masks'].sum(axis=2)).astype(np.int8))
                    #     # Compute AP
                    #     AP, precisions, recalls, overlaps =\
                    #         utils.compute_ap(gt_bbox, gt_class_id, gt_mask,
                    #                         r["rois"], r["class_ids"], r["scores"], r['masks'])
                    #     print(AP, precisions, recalls, overlaps)
                    #     APs.append(recalls)
                    # # print(np.mean(fastb))
                    # print("mAP: ", np.mean(APs))

        from postprocess import _post_process

        Data3D=f'RibFrac{rib_choice}-image.nii.gz'
        data3D = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow/Data/'+Data3D
        img_data = nib.load(data3D).get_fdata()

        new_image=_post_process(recon,img_data,0.1,0.3,400)
        fin=nib.Nifti1Image(np.float32(new_image),affine)
        nib.save(fin,f"/Users/omarhashish/Desktop/mpre/RibFrac{rib_choice}-label.nii.gz")