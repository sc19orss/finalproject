import nibabel as nib
import numpy as np
from scipy import ndimage
from skimage.measure import label, regionprops
from skimage.morphology import disk, remove_small_objects

def _remove_spine_fp(pred, image, bone_thresh):
    image_bone = image > bone_thresh
    image_bone_2d = image_bone.sum(axis=-1)
    image_bone_2d = ndimage.median_filter(image_bone_2d, 10)
    image_spine = (image_bone_2d > image_bone_2d.max() // 3)
    kernel = disk(7)
    image_spine = ndimage.binary_opening(image_spine, kernel)
    image_spine = ndimage.binary_closing(image_spine, kernel)
    image_spine_label = label(image_spine)
    max_area = 0

    for region in regionprops(image_spine_label):
        
        if region.area > max_area:
            max_region = region
            max_area = max_region.area
    image_spine = np.zeros_like(image_spine)
    image_spine[max_region.bbox[0]:max_region.bbox[2],max_region.bbox[1]:max_region.bbox[3]] = max_region.convex_image > 0

    return np.where(image_spine[..., np.newaxis], 0, pred)


def _remove_small_objects(pred, size_thresh):
    pred_bin = pred > 0
    pred_bin = remove_small_objects(pred_bin, size_thresh)
    pred = np.where(pred_bin, pred, 0)

    return pred


def _post_process(pred, image, prob_thresh, bone_thresh, size_thresh):

    # remove spine false positives
    pred = _remove_spine_fp(pred, image, bone_thresh)

    # remove small connected regions
    pred = _remove_small_objects(pred, size_thresh)

    return pred


# im_list=[10,134,135,144,15,163,177,178,196,201,228,234,249,26,260,264,274,275,285,286,292,305,309,311,315,104,105,106,119,12,122,124,130,149,150,152,155,162,197,215,224,235,238,246,255,269,27,304,320,321]
# im_list=[134]
# ind=2
# for i in im_list:
#     Data3D=f'RibFrac{i}-image.nii.gz'
#     data3D = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/kingpin2/Data/'+Data3D
#     affine= nib.load(data3D).affine
#     img_data = nib.load(data3D).get_fdata()
#     print(img_data.shape)



#     # data3D = f'/Users/omarhashish/Desktop/p/beans{ind}_Segm.nii.gz'
#     # affine= nib.load(data3D).affine
#     # img = nib.load(data3D).get_fdata()

#     data3D = f'/Users/omarhashish/Desktop/testpre/beans{ind}_Segm.nii.gz'
#     affine= nib.load(data3D).affine
#     img = nib.load(data3D).get_fdata()

#     # new_image=_post_process(img,img_data,0.1,0.3,400)
#     print(np.unique(label(new_image.astype(np.int8))))
#     fin=nib.Nifti1Image(label(new_image.astype(np.int8),background=0),affine)
#     for region in regionprops(label(new_image.astype(np.int8),background=0)):
#         print(region.area)

#     nib.save(fin,f"/Users/omarhashish/Desktop/Rib{ind}_{i}.nii.gz")
#     ind=ind+1