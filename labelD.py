import nibabel as nib
import shutil
import csv
from skimage.measure import label
import numpy as np
# im_list=[10,134,135,144,15,163,177,178,196,201,228,234,249,26,260,264,274,275,285,286,292,305,309,311,315,104,105,106,119,12,122,124,130,149,150,152,155,162,197,215,224,235,238,246,255,269,27,304,320,321]
ind=1
for i in range(475,501):
    Data3D=f'RibFrac{i}-label.nii.gz'
    dest= '/Users/omarhashish/Desktop/mtru/'+Data3D

    img= nib.load(dest)
    affine=img.affine
    img_data = img.get_fdata()
    print(img_data.shape)

    # fin=nib.Nifti1Image(label(img_data),affine)
    # nib.save(fin,f"/Users/omarhashish/Desktop/m/"+Data3D)

    f=open('/Users/omarhashish/Desktop/mtru/ribfrac-train-info-1.csv',"a")
    f.write(f'\nRibFrac{i},0,0.5,0')
    for j in np.unique(img_data):
        if j !=0:
            f.write(f'\nRibFrac{i},{int(j)},0.5,{int(j)}')
