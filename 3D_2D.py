from pickletools import float8
from patchify import patchify
import nibabel as nib
import numpy as np
from matplotlib import pyplot as plt
import glob 
from os import curdir, listdir
from os.path import isfile, join
import re
import skimage.io
maskPathOutput='/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow2D/Mask/'
imagePathOutput='/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow2D/Data/'
maskPathInput = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow/Mask/'
imagePathInput = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow/Data/'

# image='RibFrac49-image.nii.gz'

# data = '/Users/omarhashish/Desktop/IndividualProject/code/output/val/PreData/'+image
# chest_vol_data = nib.load(data).get_fdata()
def visualize_images(img1,img2):
    f = plt.figure(figsize=(15, 10))
    f.add_subplot(1,2, 1)
    plt.imshow(img1,cmap='gray')
    f.add_subplot(1,2, 2)
    plt.imshow(img2,cmap='gray')
    plt.show(block=True)
# data_m = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindowSplit/train/MaskOne/RibFrac31/Rib31-slice436.png'
# mask= skimage.io.imread(data_m)
# if  np.any(mask):
#     visualize_images(mask.astype(bool),mask.astype(bool))
import os
import cv2

#Save slice to file
def saveSlice(img, fname, path,mask):
    img = np.uint8(img * 255)
    #if the images mask is empty add character 'e' empty 
    if  np.any(mask):
        fout = os.path.join(path, f'{fname}.png')
    else:
        fout = os.path.join(path, f'{fname}e.png')
    #rotate image 90 degrees anti clockwise
    img=cv2.rotate(img,cv2.ROTATE_90_COUNTERCLOCKWISE)
    cv2.imwrite(fout, img)
    print(f'[+] Slice saved: {fout}', end='\r')

def sliceAndSaveVolumeImage(vol, fname, path,mask):
    (dimx, dimy, dimz) = vol.shape
    print(dimx, dimy, dimz)
    cnt = 0
    
    # if True:
    #     cnt += dimx
    #     print('Slicing X: ')
    #     for i in range(dimx):
    #         saveSlice(vol[i,:,:], fname+f'-slice{str(i).zfill(3)}_x', path)
            
    # if True:
    #     cnt += dimy
    #     print('Slicing Y: ')
    #     for i in range(dimy):
    #         saveSlice(vol[:,i,:], fname+f'-slice{str(i).zfill(3)}_y', path)
            
    if True:
        #loop through all slices of CT scan and call the save slice funciton
        cnt += dimz
        print('Slicing Z: ')
        for i in range(dimz):
            saveSlice(vol[:,:,i], fname+f'-slice{str(i).zfill(3)}', path,mask[:,:,i])
    return cnt


#Loop through directory and load ct scan then save it to 2D Slices
#Image
# for index, filename in enumerate(sorted(glob.iglob(imagePathInput+'*.nii.gz'))):
#     img = nib.load(filename).get_fdata()
#     numOfSlices = sliceAndSaveVolumeImage(img, 'Rib'+str(index), imagePathOutput)
#     print(f'\n{filename}, {numOfSlices} slices created \n')
#Mask
# for index, filename in enumerate(sorted(glob.iglob(maskPathInput+'*.nii.gz'))):
#     img = nib.load(filename).get_fdata()
#     numOfSlices = sliceAndSaveVolumeImage(img, 'Rib'+str(index), maskPathOutput)
#     print(f'\n{filename}, {numOfSlices} slices created \n')
########################################################################################################
#Create a directory for each CT scan and save all its slice to that directory
#Image
# for index, filename in enumerate(sorted(glob.iglob(imagePathInput+'*.nii.gz'))):
#     head,tail=os.path.split(filename)
#     File_Dir_Name=tail.split('-')[0]
#     print(File_Dir_Name)
#     num = int(re.findall(r"\d+", File_Dir_Name)[0])
#     new_dir=os.path.join(imagePathOutput+File_Dir_Name)
#     os.mkdir(new_dir)
#     img = nib.load(filename).get_fdata()
#     Mask_Name=os.path.join(maskPathInput,tail.replace("image","label"))
#     mask = nib.load(Mask_Name).get_fdata()
    
#     numOfSlices = sliceAndSaveVolumeImage(img, 'Rib'+str(num), new_dir,mask)
#     print(f'\n{filename}, {numOfSlices} slices created \n')
#Mask
# mask3D='RibFrac82-label.nii.gz'
# data3D = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow/Mask/'+mask3D
# affine= nib.load(data3D).affine
# img = nib.load(data3D).get_fdata()
# print(np.unique(img))
# print(len(np.unique(img)))
# print(img.shape)

# print("----------------------------------------------------------------------")
# mask2D='RibFrac50'
# data2D = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindowSplit/train/MaskOne/RibFrac82/'
# uniquess=[]
# # onlyfiles = [f for f in listdir(data2D) if isfile(join(data2D, f))]
# # for filename in onlyfiles:
# #     mask= skimage.io.imread(os.path.join(data2D,filename))
# #     uniquess.append(len(np.unique(mask)))
# # print(uniquess)

# recon= np.full_like(img, 1)
# for index, filename in enumerate(sorted(glob.iglob(data2D+'*.png'))):
#     m = skimage.io.imread(filename)
#     recon[:,:,index]=m

# print(np.unique(recon))
# print(len(np.unique(recon)))
# print(recon.shape)
# fin=nib.Nifti1Image(np.float32(recon),affine)
# nib.save(fin,output+"plz.nii")



import nibabel as nib
mask3D='RibFrac455-label.nii.gz'
data3D = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/DataWindow/Mask/'+mask3D
affine= nib.load(data3D).affine
img = nib.load(data3D).get_fdata()
print(np.unique(img))
visualize_images(img[:,:,150],img[:,:,150])
recon= (np.full_like(img, 0)).astype(int)
slice_num=0