import nibabel as nib
from nibabel.processing import conform
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import torchio as tio
scaler = MinMaxScaler()

#Use Windowing to filter using hounsfield units
def apply_window(img, min, max):
   img = img.clip(min,max)
   return img
   
MIN_BOUND = -100
MAX_BOUND = 1300

#normalize values so they are between 0 and 1
# def normalize(image):
    
#     image[image>MAX_BOUND] = MAX_BOUND
#     image[image<MIN_BOUND] = MIN_BOUND
#     return (image - MIN_BOUND) / (MAX_BOUND - MIN_BOUND)

def normalize(image):
    
    return (image - np.mean(image)) / (np.std(image))

# def normalize(image):
#     return pp.zscore(image,nan_policy='omit')

import os
from os import listdir
from os.path import isfile, join
data = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/Data/'
tardata = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/kingpin2/Data/'
mask = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/Mask/'
tarmask = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/kingpin2/Mask/'


#Plot graph of two images side by side
def visualize_images(img1,img2):
    f = plt.figure(figsize=(15, 10))
    f.add_subplot(1,2, 1)
    plt.imshow(img1[350,:,:],cmap='gray')
    f.add_subplot(1,2, 2)
    plt.imshow(img2[350,:,:],cmap='gray')
    plt.show(block=True)


def preprocess(srcdir,tardir):
    onlyfiles = sorted([f for f in listdir(srcdir) if isfile(join(srcdir, f))])
    #Remove MacOs file that is responsible for sorting files
    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store" )
    total=len(onlyfiles)
    count=0
    for i in onlyfiles:
        # new_i=i.rsplit('.',2)[0]
        print(str(count)+"/"+str(total))
        if count==total/2:
            break
        count=count+1
        # chest_vol = nib.load(srcdir+i)
        # chest_vol_data = chest_vol.get_fdata()
        # new_image=conform(chest_vol,out_shape=(512,512,512),voxel_size=(1,1,1))
        # affine=new_image.affine
        # new_image=new_image.get_fdata()
        # new_image=apply_window(np.float32(new_image),-100,1300)

        #Load CT scan 
        new_image = tio.ScalarImage(srcdir+i)
        transform = tio.Resample((1,1,1))
        # new_image = transform(new_image)
        # transform = tio.Resize((512,512,512))
        new_image = transform(new_image)
        affine=new_image.affine
        new_image=apply_window(np.float32(np.squeeze(new_image,axis=0)),100,2048)

        new_image=normalize(new_image)
        img=nib.Nifti1Image(new_image,affine)
        nib.save(img,tardir+i)

# 0.6351459040045738
# preprocess(data,tardata)

def mpreprocess(srcdir,tardir):
    onlyfiles = sorted([f for f in listdir(srcdir) if isfile(join(srcdir, f))])
    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store" )
    total=len(onlyfiles)
    count=0
    for i in onlyfiles:
        print(str(count)+"/"+str(total))
        if count==total/2:
            break
        count=count+1
        new_image = tio.ScalarImage(srcdir+i)
        transform = tio.Resample((1,1,1))
        new_image = transform(new_image)
        affine=new_image.affine
        new_image=((np.squeeze(new_image,axis=0))).astype(np.uint8)
        new_image[new_image>1] = 1
        img=nib.Nifti1Image(new_image,affine)
        nib.save(img,tardir+i)

# mpreprocess(mask,tarmask)

#Check which masks has an ambiguios fracture (not identified)
def check(srcdir,tardir):
    onlyfiles = sorted([f for f in listdir(srcdir) if isfile(join(srcdir, f))])
    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store" )
    total=len(onlyfiles)
    count=0
    num_amb=0
    for i in onlyfiles:
        print(str(count)+"/"+str(total))
        count=count+1
        image = tio.ScalarImage(srcdir+i)
        new_image=((np.squeeze(image.numpy(),axis=0))).astype(np.uint8)
        print(i)
        print(np.unique(new_image))
        if -1 in np.unique(new_image):
            num_amb=num_amb+1
        print(num_amb)



def average_voxel_size(srcdir,tardir):
    onlyfiles = sorted([f for f in listdir(srcdir) if isfile(join(srcdir, f))])
    #Remove MacOs file that is responsible for sorting files
    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store" )
    total=len(onlyfiles)
    count=0
    v_total=0
    print(len(onlyfiles))
    for i in onlyfiles:
        # new_i=i.rsplit('.',2)[0]
        print(str(count)+"/"+str(total))
        count=count+1
        chest_vol = nib.load(srcdir+i)
        chest_vol_data = chest_vol.get_fdata()
        vox_volume=chest_vol.header.get_zooms()[0]*chest_vol.header.get_zooms()[1]*chest_vol.header.get_zooms()[2]
        v_total=v_total+vox_volume
        print(vox_volume)
    
    print(v_total/500)
# average_voxel_size(data,tardata)

import splitfolders  # or import split_folders

input_folder = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/kingpin2/'
output_folder = '/Users/omarhashish/Desktop/IndividualProject/Rib_Fractures/mega'

# Split with a ratio.
# To only split into training and validation set, set a tuple to `ratio`, i.e, `(.8, .2)`.
splitfolders.ratio(input_folder, output=output_folder, seed=42, ratio=(.8, 0.1, 0.1), group_prefix=None) # default values